<?php

namespace App\Http\Controllers;

// Serve per accedere all'oggetto request che contiene i dati forniti dal client
use Illuminate\Http\Request;

// Serve per personalizzare il tipo di risposta HTTP
use Illuminate\Http\Response;

class TodosController extends Controller
{
    /**
     * Questo controller gestisce la tabella todos
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    // Questo metodo ritorna l'elenco dei todos in JSON
    // Viene chiamata con /api/todos [GET]
    public function list()
    {
        $results = app('db')->select("SELECT * FROM todos");
        return $results;
    }

    // Questo metodo crea un record todo nella tabella todos
    // Viene chiamata con /api/todos [POST]
    public function create(Request $request)
    {
        // Recupero il valore di description che l'utente passa in [POST] e lo metto nella variable $description
        $description = $request->input('description');
        $expireDate = $request->input('expireDate');

        if($expireDate){
            // Se viene passato expireDate eseguo la query considerando questo campo
            $results = app('db')->insert("
            INSERT INTO `todos` (`DESCRIPTION`, `DONE`, `INSERT_DATE`, `EXPIRE_DATE`)
            VALUES ('$description', '0', now(), '$expireDate')
        ");
        } else {
            // Altrimenti inserisco il todo senza expireDate
            $results = app('db')->insert("
                INSERT INTO `todos` (`DESCRIPTION`, `DONE`, `INSERT_DATE`)
                VALUES ('$description', '0', now())
            ");
        }
            return new Response(null, 200);
    }

    // Questo metodo aggiorna un record todo nella tabella todos
    // Viene chiamata con API/TODOS/{id} [PUT]
    public function update(Request $request, $id)
    {
        // Recupero il valore di description che l'utente passa in [PUT] e lo metto nella variable $description
        $description = $request->input('description');
        $done = $request->input('done');
        $expireDate = $request->input('expireDate');

        if($expireDate){
            // Se viene passato expireDate eseguo la query considerando questo campo
            $results = app('db')->update("
            UPDATE `todos`
            SET 
                `DESCRIPTION` = '$description',
                `DONE` = $done,
                `EXPIRE_DATE` = '$expireDate'
            WHERE 
                `todos`.`ID` = $id;
        ");
        } else {
            // Aggiorno senza il campo expireDate
            $results = app('db')->update("
            UPDATE `todos`
            SET 
                `DESCRIPTION` = '$description',
                `DONE` = $done,
                `EXPIRE_DATE` = null
            WHERE 
                `todos`.`ID` = $id;
        ");
        }
        // Ritorno una risposta http
        return new Response(null, 200);
    }

    // Questo metodo elimina un record todo dalla tabella todos
    // Questa viene chiamata con API/TODOS/{id} [DELETE]
    public function delete(Request $request, $id)
    {
            $results = app('db')->delete("
                DELETE FROM todos WHERE id = $id
        ");

        // Ritorno una risposta http
        return new Response(null, 204);
    }
}