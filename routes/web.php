<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

// Esempio
$router->get('/Saluto', function () use ($router) {
    return "Ciao";
}); 

// Questo ritorna l'elenco dei todos in formato JSON [GET]
$router->get('/API/TODOS', 'TodosController@list');

// Questo crea un nuovo todo e lo inserisce nella tabella todos [POST]
$router->post('/API/TODOS', 'TodosController@create');

// Questo aggiorna un todo esistente passando l'id come parametro nell'URL [PUT]
$router->put('/API/TODOS/{id}', 'TodosController@update');

// Questo elimina un todo esistente passando l'id come parametro nell'UR[DELETE]
$router->delete('/API/TODOS/{id}', 'TodosController@delete');
