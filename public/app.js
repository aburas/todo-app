console.log('File JS caricato');

async function addTodo() {
    // Qui ci sarà il codice che aggiunge i todo in db via HTTP [POST]
    var inputEl = document.getElementById("addTodoInput");
    var expireDateEl = document.getElementById("expireDateInput");

    // Recupero il valore inserito nella casella di testo
    var description = inputEl.value;
    var expireDate = expireDateEl.value;

    await fetch('/API/TODOS', {
        method: 'POST',
        // Aggiungo un header contenente il contentType perchè così il server capisce come deserializzare
        headers: {
            'Content-Type': 'application/json'
        },
        // Passo un oggetto con la proprietà description che contiene il todo
        // NB: l'oggetto deve essere serializzato
        body: JSON.stringify({
            description: description,
            expireDate: expireDate
        })
    });

    // Pulisco la casella di testo
    inputEl.value = "";
    expireDateEl.value = "";

    // Ricarico tutti i todos
    renderTodos();
}

// Questa funzione elimina un todo chiamando HTTP [DELETE] passando l'id del todo da eliminare
// API/TODOS/{id}
async function deleteTodo(id) {
    var response = await fetch('/API/TODOS/' + id, {
        method: "DELETE"
    });

    // Ricarico tutto
    renderTodos();
}

// Questa funzione salva un todo chiamando HTTP [PUT] passando l'id del todo da eliminare
// API/TODOS/{id}
async function saveTodo(id, inputDescription, expireDateEl, checkboxEl) {
    var description = inputDescription.value;
    var expireDate = expireDateEl.innerText;
    var done = checkboxEl.checked;
    var response = await fetch('/API/TODOS/' + id, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            description: description,
            done: done ? 1 : 0,
            expireDate: expireDate
        })
    });

    // Ricarico tutto
    renderTodos();
}

// Ricarica i TODOS e li stampa
async function renderTodos() {
    var ulElement = document.getElementById('todosListUl');

    // Creiamo una chiamata http verso [GET] /API/TODOS
    // AWAIT dice al browser di aspettare il risultato della fetch
    var response = await fetch('/API/TODOS');
    var todos = await response.json();

    // Eventualmente svuoto il todo, la prima volta non ce ne saranno, ma successivamente potrebbero essercene
    ulElement.innerHTML = '';

    // Faccio un classico for per stampare i todo nella pagina HTML
    for (var i = 0; i < todos.length; i++) {
        // Pesco il todo-iesimo
        var todo = todos[i];
        var liElement = document.createElement('li');
        var deleteButton = document.createElement('i');
        var saveButton = document.createElement('i');
        var divDescription = document.createElement('div');
        var inputDescription = document.createElement('input');
        var checkboxEl = document.createElement('input'); //TODO completo --> si/no
        checkboxEl.type = "checkbox"; //Lo fa diventare un checkbox
        //liElement.style = "margin: 10px 0px";
        //deleteButton.innerText = "X";
        //deleteButton.style = "margin-right: 10px";
        deleteButton.className = "mdi mdi-delete";
        inputDescription.value = todo.DESCRIPTION;
        inputDescription.type = "text";
        //inputDescription.style = "border: none";
        //saveButton.innerText = "SALVA";
        //saveButton.style = "margin-right: 10px";
        saveButton.className = "mdi mdi-content-save";
        divDescription.appendChild(deleteButton);
        divDescription.appendChild(saveButton);
        divDescription.appendChild(checkboxEl);
        divDescription.appendChild(inputDescription);
        liElement.appendChild(divDescription);

        // Quando l'utente clicca il tasto X, chiama la funzione deleteTodo
        var deleteThisTodo = deleteTodo.bind(null, todo.ID);
        deleteButton.addEventListener('click', deleteThisTodo);

        var divExpireDate = document.createElement('div');

        // Stampo la data solo se presente
        if (todo.EXPIRE_DATE != null) {
            divExpireDate.innerText = todo.EXPIRE_DATE;
            divExpireDate.className = "expireDate";
            //ivExpireDate.style = "font-size: 11px; color:red;";
        }

        //Check l'input type check quando done = 1
        if(todo.DONE === 1){
            checkboxEl.checked = true;
        }

        liElement.appendChild(divExpireDate);

        var saveThisTodo = saveTodo.bind(null, todo.ID, inputDescription,divExpireDate, checkboxEl);
        saveButton.addEventListener('click', saveThisTodo);

        ulElement.appendChild(liElement);
    }
}

// Dichiaro la funzione async che mi permette di eseguire in modo "sincrono" le istruzioni dopo l'await
async function main() {
    console.log('Documento caricato completamente');
    // Per dichiarare variabili:
    // var variabile;
    // let variabile;
    // const variabile;

    // Prendo il tag div con id = app
    var appDiv = document.getElementById('app');

    // Creo il tag input che mi permette di scrivere il nuovo todo
    var inputEl = document.createElement('input');

    // Casella di testo
    inputEl.type = "text";

    // Inserisce un'indicazione
    inputEl.placeholder = 'Inserisci il testo del todo';

    // Aggiungo l'id perchè mi servirà prenderlo successivamente
    inputEl.id = "addTodoInput";
    appDiv.appendChild(inputEl);

    // Creo il bottone che mi permette di inserire in nuovo todo
    var buttonEl = document.createElement('button');
    buttonEl.innerText = "Inserisci";

    // Creo una casella di testo per mettere la scadenza
    var expireDateEl = document.createElement('input');
    expireDateEl.id = "expireDateInput";
    expireDateEl.type = 'datetime-local';
    appDiv.appendChild(expireDateEl);

    // Bottone
    appDiv.appendChild(buttonEl);

    // Attaccio l'evento click sul bottone aggiungi
    buttonEl.addEventListener('click', addTodo);

    // Creo il tag UL
    var ulElement = document.createElement('ul');
    ulElement.id = 'todosListUl';

    // Aggiungo dentro il tag div app l'elemento ul
    appDiv.appendChild(ulElement);

    // Una volta costruita la mia interfaccia carico e reinderizzo i TODOS
    renderTodos();
}

//Aspetto che la pagina sia stata caricata completamente
document.addEventListener('DOMContentLoaded', main);